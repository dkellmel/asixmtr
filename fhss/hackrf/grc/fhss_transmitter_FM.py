#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Fhss Transmitter
# Author: Don Kellmel
# GNU Radio version: 3.8.0.0-rc2

from gnuradio import analog
from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import soapy


class fhss_transmitter(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Fhss Transmitter")

        ##################################################
        # Variables
        ##################################################
        self.vga_gain = vga_gain = 0
        self.samp_rate = samp_rate = int(2e6)
        self.rf_gain = rf_gain = 0
        self.freq = freq = 35
        self.audio_rate = audio_rate = 44.1e3

        ##################################################
        # Blocks
        ##################################################
        self.soapy_sink_0 = None
        if "hackrf" == 'custom':
            dev = 'driver=uhd'
        else:
            dev = 'driver=' + "hackrf"

        self.soapy_sink_0 = soapy.sink(1, dev, '', samp_rate, "fc32", '')

        self.soapy_sink_0.set_gain_mode(0, False)
        self.soapy_sink_0.set_gain_mode(1, False)

        self.soapy_sink_0.set_frequency(0, freq)
        self.soapy_sink_0.set_frequency(1, 100.0e6)

        # Made antenna sanity check more generic
        antList = self.soapy_sink_0.listAntennas(0)

        if len(antList) > 1:
            # If we have more than 1 possible antenna
            if len('TX') == 0 or 'TX' not in antList:
                print("ERROR: Please define ant0 to an allowed antenna name.")
                strAntList = str(antList).lstrip('(').rstrip(')').rstrip(',')
                print("Allowed antennas: " + strAntList)
                exit(0)

            self.soapy_sink_0.set_antenna(0, 'TX')

        if 1 > 1:
            antList = self.soapy_sink_0.listAntennas(1)
            # If we have more than 1 possible antenna
            if len(antList) > 1:
                if len('TX') == 0 or 'TX' not in antList:
                    print("ERROR: Please define ant1 to an allowed antenna name.")
                    strAntList = str(antList).lstrip('(').rstrip(')').rstrip(',')
                    print("Allowed antennas: " + strAntList)
                    exit(0)

                self.soapy_sink_0.set_antenna(1, 'TX')

        # Setup IQ Balance
        if "hackrf" != 'uhd' and "hackrf" != 'lime':
            if (self.soapy_sink_0.IQ_balance_support(0)):
                self.soapy_sink_0.set_iq_balance(0, 0)

            if (self.soapy_sink_0.IQ_balance_support(1)):
                self.soapy_sink_0.set_iq_balance(1, 0)

        # Setup Frequency correction
        if (self.soapy_sink_0.freq_correction_support(0)):
            self.soapy_sink_0.set_frequency_correction(0, 0)

        if (self.soapy_sink_0.freq_correction_support(1)):
            self.soapy_sink_0.set_frequency_correction(1, 0)

        if "hackrf" == 'sidekiq' or "True" == 'False':
            self.soapy_sink_0.set_gain(0, 10)
            self.soapy_sink_0.set_gain(1, 0)
        else:
            if "hackrf" == 'bladerf':
                self.soapy_sink_0.set_gain(0, "txvga1", -35)
                self.soapy_sink_0.set_gain(0, "txvga2", 0)
            elif "hackrf" == 'uhd':
                self.soapy_sink_0.set_gain(0, "PGA", 24)
                self.soapy_sink_0.set_gain(1, "PGA", 0)
            else:
                self.soapy_sink_0.set_gain(0, "PGA", 24)
                self.soapy_sink_0.set_gain(1, "PGA", 0)
                self.soapy_sink_0.set_gain(0, "PAD", 0)
                self.soapy_sink_0.set_gain(1, "PAD", 0)
                self.soapy_sink_0.set_gain(0, "IAMP", 0)
                self.soapy_sink_0.set_gain(1, "IAMP", 0)
                self.soapy_sink_0.set_gain(0, "txvga1", -35)
                self.soapy_sink_0.set_gain(0, "txvga2", 0)
                # Only hackrf uses VGA name, so just ch0
                self.soapy_sink_0.set_gain(0, "VGA", vga_gain)
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccc(
            interpolation=45,
            decimation=1,
            taps=None,
            fractional_bw=None)
        self.blocks_wavfile_source_0 = blocks.wavfile_source('wav/sound_44v1k.wav', True)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(1)
        self.analog_wfm_tx_0 = analog.wfm_tx(
            audio_rate=int(4 * audio_rate),
            quad_rate=int(4 * audio_rate),
            tau=10e-6,
            max_dev=75e3,
            fh=-1.0,
        )

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_wfm_tx_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.analog_wfm_tx_0, 0))
        self.connect((self.blocks_wavfile_source_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.rational_resampler_xxx_0, 0), (self.soapy_sink_0, 0))

    def get_vga_gain(self):
        return self.vga_gain

    def set_vga_gain(self, vga_gain):
        self.vga_gain = vga_gain
        self.soapy_sink_0.set_gain(0, "VGA", self.vga_gain)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_rf_gain(self):
        return self.rf_gain

    def set_rf_gain(self, rf_gain):
        self.rf_gain = rf_gain
        self.soapy_sink_0.set_gain(0, "AMP", self.rf_gain)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.soapy_sink_0.set_frequency(0, self.freq)

    def get_audio_rate(self):
        return self.audio_rate

    def set_audio_rate(self, audio_rate):
        self.audio_rate = audio_rate


# insert after the gnuradio hackrf_fhss_transmitter class

import os
import time
import yaml
import numpy as np


class FrequencyHopper:
    tb = None

    def __init__(self, top_block_cls=None, options=None):
        self.top_block_cls = top_block_cls
        config_file = 'fhss_config.yml'
        print(f'\nLoading config file: {config_file}')
        with open(config_file, 'r') as f:
            config_dict = yaml.safe_load(f)
        self.min_freq = float(config_dict['min_freq'])
        self.max_freq = float(config_dict['max_freq'])
        self.jump_freq = float(config_dict['jump_freq'])
        self.n_hops = int(config_dict['n_hops'])
        self.dwell = 1.0 / float(config_dict['hop_rate'])
        self.rf_gain = float(config_dict['rf_gain'])
        self.if_gain = float(config_dict['if_gain'])
        self.n_sweeps = int(config_dict['n_sweeps'])

        for key in config_dict.keys():
            print(f'{key}: {config_dict[key]}')
        print(f'dwell: {1e3 * self.dwell}ms')
        print('')

    def run(self):

        self.tb = self.top_block_cls()

        self.tb.start()

        self.tb.set_rf_gain(self.rf_gain)
        self.tb.set_vga_gain(self.if_gain)

        self.freq_hopper(n_hops=self.n_hops, dwell=self.dwell)

    def sincgars_channels(self):
        sincgars_channels = np.arange(self.min_freq, self.max_freq, self.jump_freq) \
                            + self.jump_freq / 2
        return sincgars_channels

    def freq_hopper(self, n_hops=10, dwell=250e-3):
        hop_freqs = self.sincgars_channels()
        freqs = np.divide(hop_freqs[0:n_hops], 1e6)
        # freqs = np.divide(np.random.choice(hop_freqs, n_hops), 1e6)
        freqs = [34.4e6, 34.6e6, 34.8e6, 35.0e6, 35.2e6, 35.4e6, 35.6e6, 35.8e6]
        self.tb.set_freq(34e6)
        time.sleep(100e-3)
        for i in range(self.n_sweeps):
            print(f'sweep: {i + 1}')
            for f in freqs:
                print(f'setting freq to {f:.4f}')
                self.tb.set_freq(f)
                time.sleep(dwell)


if __name__ == '__main__':
    FH = FrequencyHopper(top_block_cls=fhss_transmitter)
    FH.run()
