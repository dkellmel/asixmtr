# insert the following at the top of fhss_transmitter
# just before the other imports
import sys
sys.path.append('/usr/local/lib/python3/dist-packages')

# insert the following after the gnuradio fhss_transmitter class
# overwriting the main

import os
import time
import yaml
import argparse
import datetime
import numpy as np


class FrequencyHopper:
    tb = None

    def __init__(self, args, top_block_cls=None, options=None):
        current_datetime = datetime.datetime.now()
        print('\n\n' + 100 * '-')
        print(f'{current_datetime}')

        # f1 = open("fhss_transmitter_log.txt", "a")
        # f1.write("This is the fhss_transmitter_log file!\n")
        # f1.close()

        self.force_run = args.force
        self.top_block_cls = top_block_cls

        # config_file = '/home/asi/AsiXmtr/fhss/hackrf/fhss_config.yml'
        config_file = 'fhss_config.yml'
        print(f'\nLoading config file: {config_file}')
        with open(config_file, 'r') as f:
            config_dict = yaml.safe_load(f)
        self.auto_run = config_dict['auto_run']

        if not self.auto_run and not self.force_run:
            print(f'\n\aauto_run: {self.auto_run}\nauto_run is False\nmust run with -f argument')
            exit(0)

        # get config parameters
        self.test_run = config_dict['test_run']
        self.min_freq = float(config_dict['min_freq'])
        self.max_freq = float(config_dict['max_freq'])
        self.jump_freq = float(config_dict['jump_freq'])
        self.n_hops = int(config_dict['n_hops'])
        self.rf_gain = float(config_dict['rf_gain'])
        self.if_gain = float(config_dict['if_gain'])
        self.n_bursts = int(config_dict['n_bursts'])
        self.time_between_bursts = float(config_dict['time_between_bursts'])
        for key in config_dict.keys():
            print(f'{key}: {config_dict[key]}')

        self.dwell = 1.0 / float(config_dict['hop_rate'])
        self.test_freqs = [float(f) for f in config_dict['test_freqs']]
        self.hop_sequence = [int(n) for n in config_dict['hop_sequence']]

        print('')
        print(f'dwell: {1e3 * self.dwell}ms')
        if self.test_run:
            print(f'test_freqs: {self.test_freqs}')
        print(f'-f: {self.force_run}')
        if self.test_run is True:
            self.n_hops = len(self.test_freqs)
            print(f'\nUsing test frequencies.'
                  f'\njump_freq is ignored.'
                  f'\nmin_freq and max_freq are ignored.'
                  f'\nn_hops has been set to {self.n_hops}')

    def run(self):
        hackrf_available = False
        n_trys = 10
        sleep_time = 1
        trynum = 1
        print(f'\nChecking for HackRF.')
        while not hackrf_available and trynum <= n_trys:
            try:
                self.tb = self.top_block_cls()
                hackrf_available = True
                print(f'\nHackRF found.')
            except RuntimeError as e:
                print(f'Try {trynum}: {e}')
                time.sleep(sleep_time)
                trynum += 1
        if trynum == n_trys + 1:
            print(f'\nToo many attempts to find HackRF device.')
            exit(0)

        self.tb.set_freq(0.0)
        self.tb.start()

        self.tb.set_rf_gain(self.rf_gain)
        self.tb.set_vga_gain(self.if_gain)

        self.freq_hopper(n_hops=self.n_hops, dwell=self.dwell)

    def sincgars_channels(self):
        sincgars_channels = np.arange(self.min_freq, self.max_freq, self.jump_freq) \
                            + self.jump_freq / 2
        return sincgars_channels

    def freq_hopper(self, n_hops=10, dwell=250e-3):
        freqs = list()
        if self.test_run:
            freqs = self.test_freqs
        elif len(self.hop_sequence) == 1:
            hop_freqs = self.sincgars_channels()
            if self.hop_sequence[0] == 1:
                freqs = np.random.choice(hop_freqs, n_hops)
            elif self.hop_sequence[0] == 0:
                freqs = hop_freqs[0:n_hops]
            else:
                freqs = hop_freqs[self.hop_sequence[0]]
        else:
            hop_freqs = self.sincgars_channels()
            for n in self.hop_sequence:
                freqs.append(hop_freqs[n])
        print(f'\nfreqs: {np.divide(freqs, 1e6)} MHz')

        for i in range(self.n_bursts):
            print(f'\nsweep: {i + 1}')
            for f in freqs:
                print(f'setting freq to {f:.4f}')
                self.tb.set_freq(f)
                time.sleep(dwell)
            self.tb.set_freq(0.0)
            time.sleep(self.time_between_bursts)


def main(args):
    FH = FrequencyHopper(args, top_block_cls=fhss_transmitter)
    FH.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    manifold_filename = '8inCyl005_pedestal_manifold.npz'

    parser.add_argument('-f', '--force', required=False,
                        help='force a run ignore auto_run',
                        action='store_true')

    args = parser.parse_args()

    main(args)
